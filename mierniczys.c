#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <inttypes.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "err.h"

#ifdef DEBUG
int debug = 1;
#else
int debug = 0;
#endif

#define QUEUE_LEN 5
#define BUFFER_SIZE 1000

int main(int argc, char* argv[])
{
    int tcp_socket, msg_socket, udp_socket;
    uint16_t udp_port_n, tcp_port;
    struct sockaddr_in client_address, server_address;
    uint32_t oct_num;
    int len;
    socklen_t client_address_len;
    char buffer[BUFFER_SIZE];
    
    if (argc < 2)
        fatal("Too few arguments.\nUsage: %s tcp_port", argv[0]);
    
    if (debug) fprintf(stderr, "Serwer uruchomiony z parametrem %s\n", argv[1]);
    
    tcp_port = atoi(argv[1]);
    
    if (debug) fprintf(stderr, "Numer portu %d\n", tcp_port);
    
    /* otwarcie gniazda tcp */
    if (debug) fprintf(stderr, "Tworzenie gniazda tcp\n");
    
    tcp_socket = socket(AF_INET, SOCK_STREAM, 0);
    
    if (tcp_socket == -1)
        syserr("socket");
    
    /* ustawienie nasluchu */
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(tcp_port);
    
    if (bind(tcp_socket, (struct sockaddr *) &server_address, 
            (socklen_t) sizeof(server_address)) == -1)
        syserr("bind");
        
    if (listen(tcp_socket, QUEUE_LEN) == -1)
        syserr("listen");
    
    if (debug) fprintf(stderr, "Otwarto gniazdo tcp do nas�uchu\n");
    
    /* odbieranie polaczen od klientow */
    for ( ; ; ) {
        if (debug) fprintf(stderr, "Oczekiwanie na polaczenie\n");
        
        client_address_len = sizeof(client_address);
        msg_socket = accept(tcp_socket, (struct sockaddr *) &client_address,
                            &client_address_len);
                            
        if (msg_socket == -1)
            syserr("accept");
        
        /* Pobranie numeru portu udp */
        len = read(msg_socket, (void *) &udp_port_n, sizeof(udp_port_n));
        if (len == -1)
            syserr("read");
            
        if (debug) fprintf(stderr, "Pobrano numer portu udp: %" PRIu16 "\n", 
                            ntohs(udp_port_n));
        
        /* Zliczanie liczby oktetkow danych */
        oct_num = 0;
        
        while ((len = read(msg_socket, buffer, sizeof(buffer))) > 0) {
            /*if (debug) fprintf(stderr, "L-ba danych: %d", len);
            if (debug) fprintf(stderr, "Dane:\n%s", buffer);*/
            oct_num += len;
        }
        
        if (len == -1)
            syserr("read");
        
        if (debug) fprintf(stderr, "Odebrano %" PRIu32 " danych\n", oct_num);
        
        oct_num = htonl(oct_num);
        
        /* Zamkniecie polaczenia tcp */
        if (debug) fprintf(stderr, "Zamykanie polaczenia tcp\n");
        
        if (close(msg_socket) == -1)
            syserr("close");
        
        /* otwarcie gniazda udp */
        if (debug) fprintf(stderr, "Otwieranie gniazda udp\n");
        
        udp_socket = socket(AF_INET, SOCK_DGRAM, 0);
        
        if (udp_socket == -1)
            syserr("socket");
                
        /* zmiana nr. portu na udp */
        client_address.sin_port = udp_port_n;
        
        if (debug) fprintf(stderr, "Adres klienta: %" PRIu32 ":%" PRIu16 "\n", 
                            ntohl(client_address.sin_addr.s_addr), 
                            ntohs(client_address.sin_port));
        
        /* przeslanie podsumowania przez udp do klienta */
        if (debug) fprintf(stderr, "Przeslanie danych do klienta\n");
        
        len = sendto(udp_socket, (void*) &oct_num, sizeof(oct_num), 0, 
                    (struct sockaddr*) &client_address, sizeof(client_address));
                    
        if (len < 0)
            syserr("sendto");
        
        /* Zamkniecie gniazd */
        if (close(udp_socket) == -1)
            syserr("close");     
            
        if (debug) fprintf(stderr, "Zakonczono obsluge klienta\n");
    }
    
    return 0;
}

