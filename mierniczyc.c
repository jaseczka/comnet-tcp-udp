#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <inttypes.h>
#include <pthread.h>

#include "err.h"

#ifdef DEBUG
int debug = 1;
#else
int debug = 0;
#endif

#define BUFFER_SIZE 1000

int main(int argc, char* argv[])
{
    int tcp_socket, udp_socket;
    struct addrinfo addr_hints;
    struct addrinfo *addr_result;
    struct sockaddr_in server_address, client_address;
    uint16_t udp_port, udp_port_n, tcp_port;
    int len;
    socklen_t recv_len;
    uint32_t oct_num;
    char buffer[BUFFER_SIZE];
    
    if (argc < 4)
        fatal("Too few arguments.\nUsage: %s udp_port tcp_port server", argv[0]);
    
    if (debug) fprintf(stderr, "Klient uruchomiony z parametrami %s %s %s\n", 
                    argv[1], argv[2], argv[3]);
    
    udp_port = atoi(argv[1]);
    tcp_port = atoi(argv[2]);
    
    if (debug) fprintf(stderr, "udp: %" PRIu16 " tcp: %" PRIu16 "\n", 
                    udp_port, tcp_port);
    
    /* otwarcie gniazda tcp */
    if (debug) fprintf(stderr, "Otwieranie gniazda tcp\n");
    
    tcp_socket = socket(AF_INET, SOCK_STREAM, 0);
    
    if (tcp_socket == -1)
        syserr("socket");
        
    memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_family    = AF_INET;
    addr_hints.ai_socktype  = SOCK_STREAM;
    addr_hints.ai_protocol  = IPPROTO_TCP;
    
    if (getaddrinfo(argv[3], NULL, &addr_hints, &addr_result) != 0)
        syserr("getaddrinfo");
    
    server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = ((struct sockaddr_in*) (addr_result->ai_addr))->sin_addr.s_addr;
	server_address.sin_port = htons(tcp_port);
    
    freeaddrinfo(addr_result);
    
    if (connect(tcp_socket, (struct sockaddr *) &server_address, 
            (socklen_t) sizeof(server_address)) == -1)
        syserr("connect");  
    
    /* zmiana nr. portu na udp */
    udp_port_n = htons(udp_port);
    server_address.sin_port = udp_port_n;
    
    if (debug) fprintf(stderr, "Adres serwera: %" PRIu32 ":%" PRIu16 "\n", 
                            ntohl(server_address.sin_addr.s_addr), 
                            ntohs(server_address.sin_port));
                            
    client_address.sin_family       = AF_INET;
    client_address.sin_addr.s_addr  = htonl(INADDR_ANY);
    client_address.sin_port         = server_address.sin_port;

    /* otwarcie gniazda udp do odbierania*/
    if (debug) fprintf(stderr, "Otwieranie gniazda udp\n");
    
    udp_socket = socket(AF_INET, SOCK_DGRAM, 0);
    
    if (udp_socket == -1)
        syserr("socket");
    
    if (debug) fprintf(stderr, "Bindowanie gniazda\n");
    
    if (bind(udp_socket, (struct sockaddr *) &client_address,
			(socklen_t) sizeof(client_address)) == -1)
		syserr("bind");
    
    write(tcp_socket, &udp_port_n, sizeof(udp_port_n));
    
    /* trzeba sie upewnic ze zostal rozpoczety nasluch na udp */
    
    /* przesylanie danych z stdin */
    if (debug) fprintf(stderr, "Transmisja danych z stdin\n");
    while ((len = read(0, buffer, sizeof(buffer))) > 0)
        if (write(tcp_socket, buffer, len) == -1)
            syserr("write");
        
    if (len == -1)
        syserr("write");
        
    /* zamkniecie gniazda tcp */
    if (close(tcp_socket) == -1)
        syserr("close"); 
    
    /* odebranie danych z serwera server */
    if (debug) fprintf(stderr, "Odbieranie danych\n");
    
    recv_len = sizeof(server_address);
    len = recvfrom(udp_socket, (void*) &oct_num, sizeof(oct_num), 0, 
                (struct sockaddr *) &server_address, &recv_len);
    if (len < 0)
		syserr("error on datagram from server");
    if (len != sizeof(oct_num))
        fatal("incorrect lenght of data received");
        
    oct_num = ntohl(oct_num);
    
    /* wypisanie otrzymanych danych na stdout */
    if (debug) fprintf(stderr, "Wypisanie danych\n");
    
    printf("%" PRIu32 "\n", oct_num);
    
    /* zamkniecie gniazda udp */
    if (close(udp_socket) == -1)
        syserr("close"); 
        
    if (debug) fprintf(stderr, "Zamknieto gniazda\n");
            
    return 0;
}
