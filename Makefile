CC = gcc
CFLAGS = -Wall
TARGETS = mierniczyc mierniczys

ifeq ($(debug), 1)
	CFLAGS += -pedantic -DDEBUG -g
endif

.PHONY: all clean

all: $(TARGETS)

mierniczyc: mierniczyc.o err.o
	$(CC) $(CFLAGS) $^ -o $@

mierniczys: mierniczys.o err.o
	$(CC) $(CFLAGS) $^ -o $@

clean:
	rm -f *.o $(TARGETS)

